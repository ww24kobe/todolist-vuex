import Vue from "vue";
import Vuex from "vuex";

// 导入vuex持久化的插件
import createPersistedState from "vuex-persistedstate";

Vue.use(Vuex);

const store = new Vuex.Store({
  strict: true,
  state: {
    todos: [
      { id: "1", title: "html", isDone: false },
      { id: "2", title: "css", isDone: true },
      { id: "3", title: "vue", isDone: true },
    ],
  },
  mutations: {
    addTodo(state, title) {
      console.log("add");
      state.todos.push({ id: Date.now(), title, isDone: false });
    },
    delTodo(state, todo) {
      console.log("del");
      state.todos = state.todos.filter((item) => item !== todo);
    },
    cleanDone(state) {
      console.log("clean");
      state.todos = state.todos.filter((item) => !item.isDone);
    },
    changeDoneState(state, todo) {
      console.log("change");
      const findTodo = state.todos.find((item) => item === todo);
      findTodo.isDone = !findTodo.isDone;
    },
    updTodoTitle(state, todo) {
      console.log("updTodoTitle");
      const findTodo = state.todos.find((item) => item.id === todo.id);
      findTodo.title = todo.title;
    },
    updAllTodoStatus(state, bool) {
      console.log("updAllTodoStatus");
      state.todos.forEach((todo) => (todo.isDone = bool));
    },
  },
  getters: {
    totalNumber(state) {
      return state.todos.length;
    },
    doneNumber(state) {
      return state.todos.filter((todo) => todo.isDone).length;
    },
    isAllChecked(state) {
      let totalNumber = state.todos.length;
      let doneNumber = state.todos.filter((todo) => todo.isDone).length;
      return totalNumber === doneNumber && state.todos.length > 0;
    },
  },
  // 注册插件
  plugins: [createPersistedState()],
});

export default store;
